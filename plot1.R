#Peer Assignment 1
#list.files()  get the file name
#use read.table(data are seperated by ;)
#args(read.tablea)

data <- read.table("household_power_consumption.txt",header=T,sep=";",colClasses="character")
#head(data)
par(mar=c(4,4,2,2))
#2007-02-01 and 2007-02-02
data <- subset(data,Date=="1/2/2007" | Date=="2/2/2007")
hist(as.numeric(data$Global_active_power),col="red",main="Global Active Power",xlab="Global Active Power (Kilowatts)",ylab="Frequency",ylim=c(0,1200))
dev.copy(png,file="plot1.png",height=480,width=480)
dev.off()

